import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ReportModule } from '../src/Report/ReportModule';

describe('', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [ReportModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(404);
  });

  it('/report/products/2019-08-07 (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019-08-07')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, [
        { productName: 'Black sport shoes', totalPrice: 220, quantity: 2 },
        { productName: 'Cotton t-shirt XL', totalPrice: 25.75, quantity: 1 },
      ]);
  });

  it('/report/customer/2019-08-07 (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019-08-07')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, [
        { customerName: 'John Doe', totalPrice: 135.75 },
        { customerName: 'Jane Doe', totalPrice: 110 },
      ]);
  });
});
