import { Inject, Injectable } from '@nestjs/common';

import { OrderMapper } from '../../Order/Service/OrderMapper';
import { IBestBuyers, IBestSellers } from '../Model/IReports';
import { Product } from '../../Order/Model/Product';
import { Customer } from '../../Order/Model/Customer';
import { Order } from '../../Order/Model/Order';

@Injectable()
export class ReportService {
  @Inject() orderMapper: OrderMapper;

  async getBestSellers(date: string): Promise<IBestSellers[]> {
    const orders = await this.orderMapper.getOrdersByDate(date);

    const productsSold: Array<{ product: Product, quantity: number }> = Object.values(
      orders.reduce((result, order) => {
        for (const product of order.products) {
          if (!result[product.id]) {
            result[product.id] = { product, quantity: 0 };
          }

          result[product.id].quantity++;
        }

        return result;
      }, {}),
    );

    productsSold.sort((lhs, rhs) => rhs.quantity - lhs.quantity);

    return productsSold.map(({ product, quantity }) => ({
      productName: product.name,
      totalPrice: product.price * quantity,
      quantity,
    }));
  }

  async getBestBuyers(date: string): Promise<IBestBuyers[]> {
    const orders = await this.orderMapper.getOrdersByDate(date);

    const buyers: Array<{ customer: Customer, orders: Order[] }> = Object.values(
      orders.reduce((result, order) => {
        if (!result[order.customer.id]) {
          result[order.customer.id] = { customer: order.customer, orders: [] };
        }

        result[order.customer.id].orders.push(order);
        return result;
      }, {}),
    );

    const bestBuyers = buyers.map(({ customer, orders: customerOrders }) => ({
      customerName: customer.name,
      totalPrice: customerOrders.reduce((totalPrice, order) => totalPrice + order.totalPrice, 0),
    }));

    bestBuyers.sort((lhs, rhs) => rhs.totalPrice - lhs.totalPrice);

    return bestBuyers;
  }
}
