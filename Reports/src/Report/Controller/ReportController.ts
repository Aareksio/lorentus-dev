import { Controller, Get, Inject, Param } from '@nestjs/common';

import { IBestBuyers, IBestSellers } from '../Model/IReports';
import { ReportService } from '../Service/ReportService';

@Controller()
export class ReportController {
  @Inject() reportService: ReportService;

  @Get('/report/products/:date')
  async bestSellers(@Param('date') date: string): Promise<IBestSellers[]> {
    return this.reportService.getBestSellers(date);
  }

  @Get('/report/customer/:date')
  bestBuyers(@Param('date') date: string): Promise<IBestBuyers[]> {
    return this.reportService.getBestBuyers(date);
  }
}
