export class Product {
  id: number;
  name: string;
  price: number;

  constructor(product: Partial<Product>) {
    this.id = product.id;
    this.name = product.name;
    this.price = product.price;
  }
}
