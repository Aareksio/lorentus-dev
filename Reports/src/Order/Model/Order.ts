import { Customer } from './Customer';
import { Product } from './Product';

export class Order {
  number: number;
  customer: Customer;
  products: Product[];
  createdAt: string;

  get totalPrice() {
    return this.products.reduce((totalPrice, product) => totalPrice + product.price, 0);
  }

  constructor(order: Partial<Order>) {
    this.number = order.number;
    this.customer = order.customer;
    this.products = order.products;
    this.createdAt = order.createdAt;
  }
}
