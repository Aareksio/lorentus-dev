export class Customer {
  id: number;
  firstName: string;
  lastName: string;

  get name() {
    return `${this.firstName} ${this.lastName}`;
  }

  constructor(customer: Partial<Customer>) {
    this.id = customer.id;
    this.firstName = customer.firstName;
    this.lastName = customer.lastName;
  }
}
