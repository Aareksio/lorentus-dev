import { Injectable, Inject } from '@nestjs/common';

import { Repository } from './Repository';
import { Product } from '../Model/Product';
import { Customer } from '../Model/Customer';
import { Order } from '../Model/Order';

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;

  async getOrders() {
    const products = await this.getProducts();
    const customers = await this.getCustomers();

    const rawOrders = await this.repository.fetchOrders();
    return rawOrders.map(rawOrder => {
      return new Order({
        ...rawOrder,
        customer: customers.find(customer => customer.id === rawOrder.customer),
        products: rawOrder.products.map(productId => products.find(product => product.id === productId)),
      });
    });
  }

  async getProducts() {
    const rawProducts = await this.repository.fetchProducts();
    return rawProducts.map(rawProduct => new Product(rawProduct));
  }

  async getCustomers() {
    const rawCustomers = await this.repository.fetchCustomers();
    return rawCustomers.map(rawCustomer => new Customer(rawCustomer));
  }

  async getOrdersByDate(date: string) {
    const orders = await this.getOrders();
    return orders.filter(order => order.createdAt === date);
  }
}
