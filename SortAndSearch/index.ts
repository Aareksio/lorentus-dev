import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

const sorterA = new ASort();
const sortedA = sorterA.sort([ ...unsorted ]);
console.log(sortedA.join(', '));

const sorterB = new BSort();
const sortedB = sorterB.sort([ ...unsorted ]);
console.log(sortedB.join(', '));

const binarySearch = BSearch.getInstance();
const searchResults = elementsToFind.map(el => binarySearch.search(sortedA, el));
console.log(searchResults.join(', '));
console.log(BSearch.getInstance().operations);
