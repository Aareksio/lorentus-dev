export class ASort {
  sort(arr: number[]): number[] {
    this.quickSort(arr, 0, arr.length - 1);
    return arr; // Return for convenience
  }

  private quickSort(arr: number[], low: number, high: number): void {
    if (low < high) {
      const partitionIndex = this.partition(arr, low, high);

      this.quickSort(arr, low, partitionIndex - 1);
      this.quickSort(arr, partitionIndex + 1, high);
    }
  }

  private partition(arr: number[], low: number, high: number): number {
    const pivot = arr[high];
    let i = low;

    for (let j = low; j < high; j++) {
      if (arr[j] <= pivot) {
        this.swap(arr, i, j);
        i++;
      }
    }

    this.swap(arr, i, high);
    return i;
  }

  private swap(arr: number[], indexA: number, indexB: number): void {
    const temp = arr[indexA];
    arr[indexA] = arr[indexB];
    arr[indexB] = temp;
  }
}
