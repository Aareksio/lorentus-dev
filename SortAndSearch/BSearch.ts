export class BSearch {
  static instance: BSearch;
  private _operations = 0;

  get operations() {
    return this._operations;
  }

  search(arr: number[], key: number): number {
    const bottom = 0;
    const top = arr.length - 1;

    return this.binarySearch(arr, key, bottom, top);
  }

  private binarySearch(arr: number[], key: number, bottom: number, top: number): number {
    this._operations++;

    if (top < bottom) return -1;

    const middle = Math.floor((top + bottom) / 2);
    if (arr[middle] === key) return middle;

    if (arr[middle] < key) return this.binarySearch(arr, key, middle + 1, top);
    else return this.binarySearch(arr, key, bottom, middle - 1);
  }

  static getInstance() {
    if (BSearch.instance) return BSearch.instance;
    BSearch.instance = new BSearch();
    return BSearch.instance;
  }
}

/*
// I'd personally use module export to achieve _singleton_ purposes.
const instance = new BSearch();
export default instance;
 */
